<?php

/*****************************************************************************
 Replace feed icon with theme's feed icon.
 *****************************************************************************/
function phptemplate_feed_icon($url, $title) {
  if ($image = theme('image', path_to_theme() . '/images/rss.png', t('Syndicate content'), $title)) {
    return '<a href="'. check_url($url) .'" class="feed-icon">'. $image .'</a>';
  }
}

/*****************************************************************************
 Override theme_username() function to remove "(not verified)" from comments.
 *****************************************************************************/
function phptemplate_username($object) {
  if ($object->uid && $object->name) {
    if (drupal_strlen($object->name) > 20) {
      $name = drupal_substr($object->name, 0, 15) .'...';
    }
    else {
      $name = $object->name;
    }

    if (user_access('access user profiles')) {
      $output = l($name, 'user/'. $object->uid, array('attributes' => array('title' => t('View user profile.'))));
    }
    else {
      $output = check_plain($name);
    }
  }
  else if ($object->name) {
    if (!empty($object->homepage)) {
      $output = l($object->name, $object->homepage, array('attributes' => array('rel' => 'nofollow')));
    }
    else {
      $output = check_plain($object->name);
    }
  }
  else {
    $output = variable_get('anonymous', t('Anonymous'));
  }

  return $output;
}
