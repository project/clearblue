<div id="comment-<?php print $comment->cid ?>" class="comment<?php print ($comment->new) ? ' comment-new' : ''; print ($comment->uid == $node->uid) ? ' comment-by-author-of-post' : ''; print ' '. $status ?> clear-block">
  <div class="comment-info">
    <?php print $picture ?>
    <h3><?php print $id ?>.&nbsp;&nbsp;<?php print $author ?> (<?php print format_date($comment->timestamp, 'custom', 'j F, Y - H:i'); ?>) <?php print t('says'); ?>:</h3>
  </div>

  <div class="content">
    <?php print $content ?>

    <div class="control-links clear-block">
      <?php print $links ?>
    </div>
  </div>
</div>
