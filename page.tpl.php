<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <!--[if lte IE 6]>
    <link type="text/css" rel="stylesheet" media="all" href="<?php print $base_path ?><?php print path_to_theme() ?>/fix-ie-6.css" />
  <![endif]-->
  <?php print $scripts; ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyled Content in IE */ ?> </script>
</head>
<body>
  <div id="page-upper">
    <div id="header" class="container">
      <div id="logo-and-site-name">
        <?php if (!empty($logo)): ?>
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
            <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
          </a>
        <?php endif; ?>
        <?php if (!empty($site_name)): ?>
          <div id="site-name">
            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
              <?php print $site_name; ?>
            </a>
          </div> <!-- /#site-name -->
        <?php endif; ?>
      </div> <!-- /#logo-and-site-name -->

      <?php if (!empty($primary_links)): ?>
        <div id="primary">
          <?php print menu_tree($menu_name = 'primary-links') ?>
        </div> <!-- /#primary -->
      <?php endif; ?>
    </div> <!-- /#header -->

    <div id="main-and-sidebars" class="container clear-block">
      <div id="main">
        <?php if (!empty($breadcrumb)): ?><div id="breadcrumb"><?php print $breadcrumb; ?></div><?php endif; ?>
        <?php if (!empty($mission)): ?><div id="mission"><?php print $mission; ?></div><?php endif; ?>
        <?php if (!empty($title)): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
        <?php if (!empty($content_top)): ?>
          <div id="content-top">
            <?php print $content_top; ?>
          </div> <!-- /#content-top -->
        <?php endif; ?>
        <?php if (!empty($tabs)): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
        <?php if (!empty($messages) || !empty($help)): ?>
          <div class="clear-block">
            <?php if (!empty($messages)): print $messages; endif; ?>
            <?php if (!empty($help)): print $help; endif; ?>
          </div>
        <?php endif; ?>
        <div id="content" class="clear-block">
          <?php print $content; ?>
        </div> <!-- /#content -->
      </div> <!-- /#main -->

      <div id="sidebars">
        <?php if (!empty($left_and_right_joined)): ?>
          <div id="left-and-right-sidebar-joined">
            <?php print $left_and_right_joined; ?>
          </div> <!-- /#left-and-right-sidebar-joined -->
        <?php endif; ?>

        <div id="left-and-right-sidebar" class="clear-block">
          <div id="left-sidebar">
            <?php print $left; ?>
          </div> <!-- /#left-sidebar -->

          <div id="right-sidebar">
            <?php print $right; ?>
          </div> <!-- /#right-sidebar -->
        </div> <!-- /#left-and-right-sidebar -->
      </div> <!-- /#sidebars -->
    </div> <!-- /#main-and-sidebars -->
  </div> <!-- /#page-upper -->

  <div id="page-lower">
    <div id="footer" class="container clear-block">
      <?php if (!empty($secondary_links)): ?>
        <div id="secondary">
          <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')); ?>
        </div> <!-- /#secondary -->
      <?php endif; ?>

      <div id="footer-message">
        <?php print $footer_message; ?><br />Clearblue <?php print t('theme designed by'); ?> <a href="http://papermilkdesign.com/">Nicholas Wagner</a>
      </div> <!-- /#footer-message -->
    </div> <!-- /#footer -->
  </div> <!-- /#page-lower -->

  <?php print $closure; ?>

</body>
</html>
